package com.utm.indiawellbieng

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initialAnim()
        gotoNextScreen()
    }

    private fun initialAnim() {
        // Get the background, which has been compiled to an AnimationDrawable object.
       /* val animationDrawable = splash_main_container.background as AnimationDrawable
        animationDrawable.setEnterFadeDuration(4000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start()*/

        //App Name Animation
        val anim = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
        tv_app_name.animation = anim
        Handler().postDelayed({ anim.cancel() }, 1000)

        // App Tag Line animation
        tv_app_tag_line.text = ""
        tv_app_tag_line.setCharacterDelay(150)
        tv_app_tag_line.animateText(getString(R.string.splash_app_tag_line))



    }
    @SuppressLint("WrongConstant")
    private fun setIncompleteCardAnimation() {
        val anim = ObjectAnimator.ofInt(
            tv_app_name,
            "backgroundColor",
            Color.WHITE,
            Color.RED,
            Color.WHITE
        )
        anim.duration = 3000
        anim.setEvaluator(ArgbEvaluator())
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE
        anim.start()
    }
    private fun gotoNextScreen(){
        Handler().postDelayed({
            val intent = Intent(this, WelcomeActivity::class.java)
            startActivity(intent)
            finish()
        }, 4000)
    }
}
