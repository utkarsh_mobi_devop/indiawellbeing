package com.utm.indiawellbieng.services

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiCallBack(private val apiListener: ApiResponseListener, private val mContext: Context) :
    Callback<ResponseBody> {

    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {

        Log.e("Response",Gson().toJson(response.body()))
        apiListener.onApiSuccess(null)

}

override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
    Log.e("Response",Gson().toJson(t.message))
    apiListener.onApiFailure(null)
}

}