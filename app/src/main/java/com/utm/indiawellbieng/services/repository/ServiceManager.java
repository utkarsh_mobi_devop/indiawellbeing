package com.utm.indiawellbieng.services.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.utm.indiawellbieng.services.ApiCallBack;
import com.utm.indiawellbieng.services.ApiClient;
public class ServiceManager extends ApiClient {


    public ServiceManager() {
        super();
    }

    public void callBaseAuth(ApiCallBack apiCallBack ) {
        ApiClient.current().getListApi().enqueue(apiCallBack);
        Log.e("URL===>>", String.valueOf(ApiClient.current().getListApi().request().url()));

    }

}
