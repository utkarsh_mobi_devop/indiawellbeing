package com.utm.indiawellbieng.services

import com.google.gson.GsonBuilder
import com.utm.indiawellbieng.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class ApiClient {
    private val service: Api

    companion object {
        private var BASE_URL: String? = null
        private var _client: ApiClient? = null
        @JvmStatic
        fun current(): Api {
            return instance!!.service
        }

        private val instance: ApiClient?
            private get() {
                if (_client == null) {
                    _client = ApiClient()
                }
                return _client
            }

        init {
            BASE_URL = if (BuildConfig.DEBUG) {
                "https://api.covid19india.org/"
            } else {
                "https://api.covid19india.org/"
            }
        }
    }

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(interceptor)
        httpClient.connectTimeout(2 * 60 * 1000.toLong(), TimeUnit.SECONDS)
        httpClient.readTimeout(2 * 60 * 1000.toLong(), TimeUnit.SECONDS)
            .writeTimeout(2 * 60 * 1000.toLong(), TimeUnit.SECONDS)
        httpClient.addInterceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            val request: Request
            request = original.newBuilder()
                .header("Content-Type", "application/json")
                .method(original.method(), original.body()).build()
            chain.proceed(request)
        }
        val gson = GsonBuilder()
            .setLenient()
            .create()
        val client = httpClient.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()
        service =
            retrofit.create(Api::class.java)
    }
}