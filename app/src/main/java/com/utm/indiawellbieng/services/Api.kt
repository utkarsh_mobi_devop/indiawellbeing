package com.utm.indiawellbieng.services

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET


interface Api {
    @GET("data.json")
    fun getListApi(): Call<ResponseBody?>?

 }