package com.utm.indiawellbieng.services

interface ApiResponseListener {
    fun <T> onApiSuccess(response: T)
    fun <T> onApiFailure(errorResponse: T)
}