package com.jr.digitalchunav.services

interface StatusCode {
    companion object {
        const val SUCCESS = "Success"
        const val TRUE = "true"
        const val BLANK_REQUEST = "100"
        const val VALIDATION_ERROR = "300"
        const val DB_ERROR = "400"
        const val INVALID_API_KEY = "500"
        const val INACTIVE_USER = "600"
        const val NOT_FOUND = "404"
        const val DATA_ALREADY_EXIST = "0"
        const val SESSION_EXPIRED = "240"
        const val INVALID_USER_NAME_OR_PASSWORD = "502"
    }
}