package com.utm.indiawellbieng.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.utm.indiawellbieng.R
import com.utm.indiawellbieng.services.ApiCallBack

/**
 * A simple [Fragment] subclass.
 */
class FeedsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_feeds, container, false)
    }


}
