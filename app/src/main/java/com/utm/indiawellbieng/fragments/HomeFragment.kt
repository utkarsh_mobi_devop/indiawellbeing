package com.utm.indiawellbieng.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.utm.indiawellbieng.R
import com.utm.indiawellbieng.services.ApiCallBack
import com.utm.indiawellbieng.services.ApiResponseListener
import com.utm.indiawellbieng.services.repository.ServiceManager

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {
private lateinit var mContext:Context

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        ServiceManager().callBaseAuth(ApiCallBack(coronaStatusListener,mContext))
         return inflater.inflate(R.layout.fragment_home, container, false)
    }

    private val coronaStatusListener = object : ApiResponseListener {
        override fun <T> onApiSuccess(response: T) {

        }

        override fun <T> onApiFailure(errorResponse: T) {

        }

    }
}
