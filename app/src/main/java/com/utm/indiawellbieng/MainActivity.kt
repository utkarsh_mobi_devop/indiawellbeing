package com.utm.indiawellbieng

import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.utm.indiawellbieng.adapter.ViewPagerAdapter
import com.utm.indiawellbieng.fragments.FeedsFragment
import com.utm.indiawellbieng.fragments.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.footer_layout.*

class MainActivity : AppCompatActivity() {
    private val tabIcons: IntArray = intArrayOf(R.drawable.ic_covid, R.mipmap.ic_launcher_round)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpFooterLayout()
        setUpTabLayout()
    }

    private fun setUpTabLayout() {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(HomeFragment(), "COVID 19")
        adapter.addFragment(FeedsFragment(), "FEEDS")
        viewPager.adapter = adapter
        tab_main_layout.setupWithViewPager(viewPager)
        setupTabIcons()
    }
    private fun setupTabIcons() {
        tab_main_layout.getTabAt(0)!!.setIcon(tabIcons[0])
        tab_main_layout.getTabAt(1) !!.setIcon(tabIcons[1])

    }
    private fun setUpFooterLayout() {
        val anim = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
        ll_footer.animation = anim
        Handler().postDelayed({ anim.cancel() }, 1000)
        tv_your_state.text = getString(R.string.txt_label_header)
        tv_label_confirmed.text = getString(R.string.txt_label_confirmed)
        tv_label_recovered.text = getString(R.string.txt_label_recovered)
        tv_label_desceased.text = getString(R.string.txt_label_desceased)
    }


}
